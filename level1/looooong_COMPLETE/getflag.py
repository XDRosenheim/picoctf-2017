#!/usr/bin/env python3

from pwn import *

host = 'shell2017.picoctf.com'
port = '14319'

conn = remote(host, port)

conn.recvline() # Skip that first line
challenge = conn.recvline()
conn.recvline() # Skip that line, too
conn.recvline()

# Free the text we need
challenge = re.split(r'\'(\w|\d\d\d|\d)\'', challenge)

# Send the challenge
conn.sendline(str(challenge[1]) * int(challenge[3]) + str(challenge[-2]))

conn.recvline() # Skip the happy

# Grab the flag
print conn.recvline()[6:]

conn.close()
