# Creating a ssh key
Using the following command, on your own computer, to create a ssh key, which can be used to connect to PicoCTF's shell.
```bash
ssh-keygen -t rsa -C "your.name@example.com"
```
- `-C "text"` to leave a comment on your key.

You will be told to enter a file name and where to save your key.    
I like to name my keys after what they will be used for so I name mine something like: `picoexample`
Give your key a passphrase and you should get some output that looks something like this:
```bash
xdrosenheim@homebox:~$ ssh-keygen -t rsa -C "example"
Generating public/private rsa key pair.
Enter file in which to save the key (/home/xdrosenheim/.ssh/id_rsa): picoexample
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in picoexample.
Your public key has been saved in picoexample.pub.
The key fingerprint is:
SHA256:8yk70cxGE/A/bEBXINIFDB+gRSO7Ln5heJgV8odtvJQ example
The key's randomart image is:
+---[RSA 2048]----+
|  ..B=+o+=o      |
| . * +o.+o.      |
|  = = o   o.     |
|   = E    o+     |
|  * + . S= .=    |
| = + .  .o=o .   |
|. + .   .oo      |
|.. .    .o       |
| ..     ..       |
+----[SHA256]-----+
```
Two keys will now be generated inside `~/.ssh`, a private key called `picoexample` and a public key called `picoexample.pub`.
# Giving away your public keys
Since you don't have access to PicoCTF outside of the site itself, you have to give them your public key manually.   
Use the following command, on your own computer, to copy the public key to your clipboard
```bash
xclip -sel clip < ~/.ssh/picoexample.pub
```
Goto the PicoCTF challenge and click the `Connect` button in the challenge.
Make the ssh directory in your home folder using `mkdir .ssh` and navigate into it with `cd .ssh`.  
Use `nano authorized_keys` (Or any other editor, like `vi`), paste in the public key and save the file.

From your own computer, you should now be able to connect to to PicoCTF, using your private key.

```bash
ssh -i .ssh/picoexample xdrosenheim@shell2017.picoctf.com
```

Your flag will be displayed when you have successfully logged in.
